//  Alert span text content on click - do no bubble
const alertSpan = function (event) {
  event.preventDefault()
  event.stopPropagation()

  var title = this.querySelector('span')
  alert(title.textContent)
}

//  Watch for clicks
document.querySelectorAll('div.click').forEach((item, i) => {
  item.addEventListener( 'mouseup', alertSpan )
})
